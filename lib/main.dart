import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Solid Software',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      debugShowCheckedModeBanner: false,
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Color bgColor = Colors.white;

  void changeBg() {
    int red = Random().nextInt(256) + 1;
    int green = Random().nextInt(256) + 1;
    int blue = Random().nextInt(256) + 1;
    Color newColor = Color.fromRGBO(red, green, blue, 1);
    setState(() {
      bgColor = newColor;
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: changeBg,
      child: Scaffold(
        backgroundColor: bgColor,
        body: const Center(
          child: Text('Hello there'),
        ),
      ),
    );
  }
}
